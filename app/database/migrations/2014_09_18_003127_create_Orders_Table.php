<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->bigInteger('user_id')->unsigned();
			$table->decimal('total', 10,2);
			$table->text('shipping_address');
			$table->text('billing_address');
			$table->enum('order_status', ['placed', 'confirmed', 'shipped'])->default('placed');
			$table->enum('payment_status', ['unpaid', 'paid', 'refunded'])->default('unpaid');
			$table->boolean('void');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}