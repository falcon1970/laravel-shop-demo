<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $table = 'users';

	protected $fillable = ['email','password'];

	static $rules = [
		'email' => 'email|unique:users,email|required',
		'password' => 'min:8|confirmed|required'
	];

	static $rulesMessages = [
		'email.required'     => "We need your email.",
		'email.unique'       => "That email address already has an account",
		'email.email'        => "This doesn't look like an email.",
		'password.required'  => "A password is required.",
		'password.min'       => "Passwords should be at least 8 characters long.",
		'password.confirmed' => "Passwords need to match.",
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function orders()
	{
		return $this->hasMany('Order');
	}

}
