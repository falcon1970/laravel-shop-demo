<?php

class CartController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('auth');
	}

	/**
	 * Display a listing of the resource.
	 * GET /cart
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = UserProductCart::currentUser();

		$total = $products->cartTotal();

		return View::make('cart.index', compact('products', 'total'));
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /cart
	 *
	 * @return Response
	 */
	public function store()
	{
		$user_id = Auth::id();
		$product_id = Input::get('product_id');
		$quantity = Input::get('quantity', 1);

		$data = compact('user_id', 'product_id', 'quantity');

		$validator = Validator::make($data, UserProductCart::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		
		// find existing matching product in cart
		$cart_entry = UserProductCart::where('user_id', $user_id)->where('product_id', $product_id)->first();

		if ($cart_entry) {
			$cart_entry->quantity += $quantity;
			$cart_entry->save();
		} else {
			$cart_entry = UserProductCart::create(
				compact('user_id', 'product_id', 'quantity')
			);
		}

		return Redirect::route('cart.index');
	}

	/**
	 * Display the specified resource.
	 * GET /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /cart/
	 *
	 * @return Response
	 */
	public function update()
	{
		$userCart = UserProductCart::currentUser();
		// loop over all the input fields
		foreach (Input::all() as $key => $quantity) {
			// if the key starts with 'quantity-cart-'
			if (strpos($key, 'quantity-cart-') > -1) {
				// get the id
				$cart_id = explode('-', $key)[2];
				// update the quantity of that product in the user cart
				$cartitem = $userCart->find($cart_id);
				$cartitem->quantity = floor($quantity);
				$cartitem->save();
			}
		}

		return Redirect::route('cart.index');
	}

	public function checkout()
	{
		// collect addresses for new order
		// display a form
	}

	public function sendToPaymentProcessor()
	{
		// validate address
		// redirect if not valid

		// copy cart contents to new order
		$cart = UserProductCart::currentUser();

		$order = Order::makeFromCart($cart);
		return $order;

		// save order id to session
		//send to DPS

	}

	public function verifyWithPaymentProcessor()
	{
		// check if DPS says it's accepted or not
		// load order from session order id
		// update order to mark as paid and confirmed
		// tell the user
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /cart/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}